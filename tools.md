# parts

TODO review [this list][0] of tools

## tool kit

good [overview](http://www.advrider.com/forums/showthread.php?t=262998) of suggested parts here

### wrenches

- [x] 8mm
- [x] 10mm
- [x] 12mm
- [x] 13mm
- [x] 14mm
- [x] 17mm
- [x] 24mm

### hex wrenches

- [x] 4mm
- [x] 5mm
- [x] 6mm

### sockets

- [x] 1/4" driver
- [x] 8mm
- [x] 10mm
- [x] 12mm
- [x] 13mm
- [x] 14mm

### drivers

- [x] flat head
- [x] philips

### tires

- [x] tubes
- [x] tire irons (2-3)
- [x] patches
- [x] glue
- [x] pump / air compressor
- [x] soap

### other

- [x] spark plug puller
- [x] spark plug
- [x] light
- [x] pliers
- [x] large zip ties
- [x] rag
- [x] spare fuel line
- [x] quicksteel / jb weld
- [x] 2' electrical wire
- [x] electrical tape
- [x] duct tape
- [x] chain tool
- [x] short length of extra chain & master link

[0]: https://advrider.com/f/threads/the-toolkit-thread.262998/page-219#post-36251657
