# 2010 KTM 690 Enduro R

Purchased on **10/04/17** with **16850** miles for \$6900

## maintenance

| date       | miles | tasks                                                                                                                 |
| ---------- | ----- | --------------------------------------------------------------------------------------------------------------------- |
| 2019-01-10 | 26581 | install new rocker arms (intake & exhaust) & reshim (exhaust: left 2.50, right 2.50; intake: left 2.50, right 2.40)   |
| 2018-12-09 | 26581 | change coolant (Engine Ice); bleed clutch fluid                                                                       |
| 2018-12-05 | 26581 | install OEM gear sensor and o-ring                                                                                    |
| 2018-11-19 | 26581 | change oil, clean oil filters and screens (water in oil from PB500); check valves; inspect rocker arms and cam lobes; |
| 2018-10-19 | 26112 | install D606 130/90 rear and 216MX 90/90 front tires; install side stand switch                                       |
| 2018-10-14 | 26112 | install Rally Raid manual cam chain tensioner                                                                         |
| 2018-10-03 | 26112 | install EBC rear brake pads                                                                                           |
| 2018-09-14 | 25345 | install CA Cycleworks fuel pump and NAPA 3011 fuel filter                                                             |
| 2018-09-13 | 24930 | change oil, clean oil filters and screens                                                                             |
| 2018-09-07 | 23500 | install D606 120/90 rear and 216MX Fatty 90/100 front tires                                                           |
| 2018-09-07 | 23200 | install KTM fuel injector and Uni UP-6245AST air filter                                                               |
| 2018-08-30 | 21745 | change oil, clean oil filters and screens                                                                             |
| 2018-08-29 | 21485 | install TKC80 rear tire                                                                                               |
| 2018-08-04 | 18633 | install IRC heavy duty tubes, D606 130/90 rear and MT21 90/90 front tires; install OEM front and rear brake pads      |
| 2018-08-02 | 18633 | install Mahle KL 15 fuel filter, Rally Raid FI hardware and new fuel hose clamps                                      |
| 2018-08-01 | 18633 | install 9.4 rear spring, 6.0 fork springs and Superplush preload adjuster @ 20mm preload                              |
| 2018-06-25 | 18628 | check valve clearances                                                                                                |
| 2018-06-18 | 18628 | change oil, install Scott's steel oil filters, and clean screens                                                      |
| 2018-06-13 | 18620 | install Hammerhead shift lever                                                                                        |
| 2018-06-06 | 18620 | install Uni UP-6245AST air filter and Outerwears pre-filter; install inline fuel filter for auxiliary tank            |
| 2018-05-15 | 18370 | install Motominded Baja Design Squadron Pro Kit, LED front turn signals and relay                                     |
| 2018-02-24 | 18100 | install HDB guards, Oberon clutch slave cylinder, 520 VX2 X-Ring chain, Rally Raid chain guard, side stand            |
| 2018-12-15 | 18000 | install Perun rack and heel guards                                                                                    |
| 2017-10-21 | 17380 | install 520 VX2 X-Ring chain, 15T countershaft and 45T rear sprockets                                                 |
| 2017-10-04 | 16850 | purchase :)                                                                                                           |

## valve clearances

2019-01-10

|         | R      | L      |
| ------- | ------ | ------ |
| intake  | 0.10mm | 0.10mm |
| exhaust | 0.09mm | 0.09mm |

2018-11-19

|         | R      | L      |
| ------- | ------ | ------ |
| intake  | 0.08mm | 0.08mm |
| exhaust | 0.09mm | 0.13mm |

2018-06-25

|         | R      | L      |
| ------- | ------ | ------ |
| intake  | 0.09mm | 0.09mm |
| exhaust | 0.09mm | 0.10mm |

#motoref
