# 2016 triumph tiger 800 xcx history

purchased on **08/18/16** with **2** miles

## maintenance

| date       | miles | tasks                                                                 |
| ---------- | ----- | --------------------------------------------------------------------- |
| 2017-09-23 | 6830  | install bar risers                                                    |
| 2017-09-03 | 6321  | change oil & install new filter                                       |
| 2017-08-09 | 5380  | install bark busters                                                  |
| 2017-08-08 | 5370  | replace tires (Mitas E-07 Dakar)                                      |
| 2016-11-02 | 2100  | install heated grips (service by Ducati-Triumph NYC)                  |
| 2016-08-29 | 548   | first service; oil change, etc. (service by National Powersports, NH) |
| 2016-08-23 | 300   | install altrider rear rack                                            |
| 2016-08-18 | 2     | purchase :)                                                           |

#motoref
