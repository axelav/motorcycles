# motorcycles

maintainence histories, notes & an [issue tracker](https://github.com/axelav/motorcycles/issues) for my bikes.

## current

- [2010 KTM 690 Enduro R](690enduro)
- [2016 Triumph Tiger XCx](tiger800xcx)
- [1986 Honda XR600R](xr600r)

## previous

- [2008 Honda XR650L](previous/xr650l/)
- [1983 Honda XL600R](previous/xl600r/)
